﻿CREATE TABLE [dbo].[Camiseta]
(
	[Id] BIGINT NOT NULL PRIMARY KEY, 
    [Nome] VARCHAR(100) NOT NULL, 
    [Descricao] VARCHAR(250) NOT NULL, 
    [ImagemId] BIGINT NOT NULL, 
    [Cor] TINYINT NOT NULL, 
    CONSTRAINT [FK_Camiseta_Imagem] FOREIGN KEY ([ImagemId]) REFERENCES [Imagem]([Id])
)
