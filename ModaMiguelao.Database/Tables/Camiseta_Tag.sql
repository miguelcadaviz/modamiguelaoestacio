﻿CREATE TABLE [dbo].[CamisetaTag]
(
	[Id] BIGINT NOT NULL PRIMARY KEY, 
	[CamisetaId] BIGINT NOT NULL, 
    [Descricao] VARCHAR(100) NOT NULL, 
    CONSTRAINT [FK_CamisetaTag_Camiseta] FOREIGN KEY ([CamisetaId]) REFERENCES [Camiseta]([Id]), 
)
