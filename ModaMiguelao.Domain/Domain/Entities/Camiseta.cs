﻿using ModaMiguelao.Domain.Domain.Enums;
using ModaMiguelao.Framework.Domain.Entities;
using System.Collections.Generic;

namespace ModaMiguelao.Domain.Domain.Entities
{
    public class Camiseta : EntityBase
    {
        public string Nome { get; set; }

        public string Descricao { get; set; }

        public Imagem Imagem { get; set; }

        public Cor Cor { get; set; }

        public List<string> Tags { get; set; }

        public Camiseta()
        {

        }
    }
}
