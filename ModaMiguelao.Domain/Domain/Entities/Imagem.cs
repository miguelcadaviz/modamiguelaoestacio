﻿using ModaMiguelao.Framework.Domain.Entities;

namespace ModaMiguelao.Domain.Domain.Entities
{
    public class Imagem : EntityBase
    {
        public string Nome { get; set; }
        public string Url { get; set; }
    }
}
