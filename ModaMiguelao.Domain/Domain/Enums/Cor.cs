﻿namespace ModaMiguelao.Domain.Domain.Enums
{
    public enum Cor : short
    {
        Branca = 0,
        Preta,
        Azul,
        Vermelha,
        Verde,
        Cinza,
        Amarela,
    }
}
