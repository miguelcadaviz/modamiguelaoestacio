﻿using ModaMiguelao.Domain.Domain.Entities;
using ModaMiguelao.Framework.Repositories;
using System.Collections.Generic;
using System.Linq;

namespace ModaMiguelao.Domain.Repositories
{
    public class CamisetaRepository : RepositoryBase<Camiseta>
    {
        public CamisetaRepository(DatabaseContext database)
            : base(database)
        { }

        public Camiseta Obter(long id)
        {
            var camiseta = Query<Camiseta, Imagem, Camiseta>(ResourcesModaMiguelaoSql.Camiseta.Obter,
                (camisetaObj, imagem) =>
                {
                    camisetaObj.Imagem = imagem;
                    return camisetaObj;
                },
                new
                {
                    Id = id
                }).SingleOrDefault();

            if (camiseta != null)
            {
                camiseta.Tags = ObterTags(camiseta.Id).ToList();
            }

            return camiseta;
        }
        
        public Camiseta Salvar(Camiseta camiseta)
        {
            camiseta.Id = ExecuteScalar<long>(ResourcesModaMiguelaoSql.Camiseta.Salvar,
                                   new
                                   {
                                       Id = camiseta.Id,
                                       Nome = camiseta.Nome,
                                       Descricao = camiseta.Descricao,
                                       Cor = camiseta.Cor,
                                       ImagemId = camiseta.Imagem.Id,
                                   });

            SalvarTags(camiseta);
            return camiseta;
        }

        private void SalvarTags(Camiseta camiseta)
        {
            foreach (var tag in camiseta.Tags)
            {
                ExecuteScalar<long>(ResourcesModaMiguelaoSql.CamisetaTags.Salvar,
                                   new
                                   {
                                       Id = camiseta.Id,
                                       Descricao = tag,
                                   });
            }
        }

        private ICollection<string> ObterTags(long camisetaId)
        {
            return Query<string>(ResourcesModaMiguelaoSql.CamisetaTags.Obter,
                new
                {
                    CamisetaId = camisetaId
                });
        }
    }
}
