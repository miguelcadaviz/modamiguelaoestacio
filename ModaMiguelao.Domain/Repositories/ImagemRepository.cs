﻿using ModaMiguelao.Domain.Domain.Entities;
using ModaMiguelao.Framework.Repositories;
using System.Collections.Generic;

namespace ModaMiguelao.Domain.Repositories
{
    public class ImagemRepository : RepositoryBase<Imagem>
    {
        public ImagemRepository(DatabaseContext database)
            : base(database)
        { }

        public ICollection<Imagem> Listar()
        {
            return Query<Imagem>(ResourcesModaMiguelaoSql.Imagem.Listar);
        }
    }
}