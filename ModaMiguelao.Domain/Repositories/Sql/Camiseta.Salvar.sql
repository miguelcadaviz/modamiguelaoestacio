﻿MERGE [Camiseta] AS target
USING (SELECT @Id) AS source (Id)
	ON (target.Id = source.Id AND source.Id IS NOT NULL)
WHEN MATCHED THEN
	UPDATE
	SET
		[Nome] = @Nome, 
		[Descricao] = @Descricao, 
		[Cor] = @Cor, 
		[ImagemId] = @ImagemId
WHEN NOT MATCHED THEN
	INSERT (
		[Nome], 
		[Descricao], 
		[Cor], 
		[ImagemId]
	) VALUES (
		@Nome,
		@Descricao,
		@Cor,
		@ImagemId
	);

SELECT IIF(@Id > 0, @Id, SCOPE_IDENTITY())
