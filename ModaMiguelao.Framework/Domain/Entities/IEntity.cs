﻿namespace ModaMiguelao.Framework.Domain.Entities
{
    public interface IEntity
    {
        long Id { get; set; }
        bool IsNew { get; }
    }
}
