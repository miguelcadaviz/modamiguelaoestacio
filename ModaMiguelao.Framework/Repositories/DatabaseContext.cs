﻿using System;
using System.Data;
using System.Data.SqlClient;

namespace ModaMiguelao.Framework.Repositories
{
    public class DatabaseContext : IDisposable
    {
        private bool disposedValue = false; // To detect redundant calls
        private SqlConnection m_connection = null;
        private SqlTransaction m_transaction = null;
        private bool _ignoreTransaction;

        /// <summary>
        /// Inicia uma nova instância da classe <see cref="DatabaseContext"/>.
        /// </summary>
        /// <param name="connectionString">A string de conexão.</param>
        public DatabaseContext(string connectionString)
        {
            this.ConnectionString = connectionString;
        }

        /// <summary>
        /// Inicia uma nova instância da classe <see cref="DatabaseContext"/>.
        /// </summary>
        /// <param name="connectionString">A string de conexão.</param>
        public DatabaseContext(string connectionString, bool ignoreTransaction) : this(connectionString)
        {
            _ignoreTransaction = ignoreTransaction;
        }

        /// <summary>
        /// Finaliza uma instância da classe <see cref="DatabaseContext" />.
        /// </summary>
        ~DatabaseContext()
        {
            Dispose(false);
        }

        /// <summary>
        /// Obtém ou define a string de conexão.
        /// </summary>
        public string ConnectionString { get; set; }

        /// <summary>
        /// Quando true, o commit sempre será realizado antes do database sofrer Dispose.
        /// </summary>
        public bool ForcePersistence { get; set; }

        /// <summary>
        /// Obtém a conexão com a base de dados (criada caso ainda não exista).
        /// </summary>
        public SqlConnection Connection
        {
            get
            {
                if (null == m_connection)
                {
                    m_connection = new SqlConnection(this.ConnectionString);
                }

                return m_connection;
            }
        }

        /// <summary>
        /// Obtém a transação usada pela conexão nesta requisição (criada caso não exista).
        /// </summary>
        public virtual SqlTransaction Transaction
        {
            get
            {
                if (!_ignoreTransaction && null == m_transaction)
                {
                    var connection = this.Connection;

                    if (connection.State != ConnectionState.Open)
                    {
                        connection.Open();
                    }

                    m_transaction = connection.BeginTransaction();
                }

                return m_transaction;
            }
        }

        /// <summary>
        /// Realiza Commit da transação atual.
        /// </summary>
        public void Commit()
        {
            Transaction.Commit();
        }

        /// <summary>
        /// Realiza RollBack da transação atual.
        /// </summary>
        public void Rollback()
        {
            Transaction.Rollback();
        }

        #region IDisposable Support
        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing && null != m_connection)
            {
                m_connection.Dispose();
                m_connection = null;
            }
        }
        #endregion
    }
}
