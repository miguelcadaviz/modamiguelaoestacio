﻿using Dapper;
using ModaMiguelao.Framework.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace ModaMiguelao.Framework.Repositories
{
    public abstract class RepositoryBase<TEntity> where TEntity : IEntity
    {
        private readonly DatabaseContext _context;

        public RepositoryBase(DatabaseContext dbContext)
        {
            _context = dbContext;
        }

        protected T ExecuteScalar<T>(string sql, object param = null)
        {
            return _context.Connection.ExecuteScalar<T>(sql, param, transaction: _context.Transaction, commandType: CommandType.Text);
        }

        protected void ExecuteNonQuery(string sql, object param = null)
        {
            _context.Connection.Execute(sql, param, transaction: _context.Transaction, commandType: CommandType.Text);
        }

        protected IDataReader ExecuteReader(string sql, object param = null)
        {
            return _context.Connection.ExecuteReader(sql, param, transaction: _context.Transaction, commandType: CommandType.Text);
        }

        protected ICollection<T> Query<T>(string sql, object param = null)
        {
            return _context.Connection.Query<T>(sql, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }

        protected ICollection<TReturn> Query<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeventh, TReturn> map, object param = null)
        {
            return _context.Connection.Query(sql, map, param, transaction: _context.Transaction, commandType: CommandType.Text).ToList();
        }
    }
}
