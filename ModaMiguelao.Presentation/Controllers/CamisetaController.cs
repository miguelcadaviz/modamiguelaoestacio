﻿using ModaMiguelao.Domain.Repositories;
using ModaMiguelao.Framework.Repositories;
using ModaMiguelao.Presentation.Mappers;
using ModaMiguelao.Presentation.Models;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ModaMiguelao.Presentation.Controllers
{
    public class CamisetaController : Controller
    {
        CamisetaRepository _camisetaRepository;
        public CamisetaController()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ModaMiguelao"].ConnectionString;
            _camisetaRepository = new CamisetaRepository(new DatabaseContext(connectionString));
        }
        public ActionResult Index()
        {
            return View();
        }

        //
        // POST: /Camiseta/Cadastrar
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Cadastrar(CamisetaViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                var model = viewModel.Mapper();
                model = _camisetaRepository.Salvar(model);

                viewModel = model.Mapper();
            }

            // If we got this far, something failed, redisplay form
            return View(viewModel);
        }

       

    }
}