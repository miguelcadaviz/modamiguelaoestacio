﻿using ModaMiguelao.Domain.Repositories;
using ModaMiguelao.Framework.Repositories;
using ModaMiguelao.Presentation.Mappers;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace ModaMiguelao.Presentation.Controllers
{
    public class ImagemController : Controller
    {
        ImagemRepository _imagemRepository;
        public ImagemController()
        {
            var connectionString = ConfigurationManager.ConnectionStrings["ModaMiguelao"].ConnectionString;
            _imagemRepository = new ImagemRepository(new DatabaseContext(connectionString));
        }
        public ActionResult Index()
        {
            return View();
        }

        //
        // Get: /Imagem/Listar
        [HttpGet]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Listar()
        {
            var imagens = _imagemRepository.Listar();
            var imagensViewModel = imagens.Select(x => x.Mapper());

            return View(imagensViewModel);
        }
    }
}