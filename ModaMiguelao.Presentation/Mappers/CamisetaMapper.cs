﻿using ModaMiguelao.Domain.Domain.Entities;
using ModaMiguelao.Presentation.Models;
using System.Linq;
using System.Text;

namespace ModaMiguelao.Presentation.Mappers
{
    public static class CamisetaMapper
    {
        public static Camiseta Mapper(this CamisetaViewModel viewModel)
        {
            return new Camiseta
            {
                Id = viewModel.Id,
                Nome = viewModel.Nome,
                Descricao = viewModel.Descricao,
                Cor = viewModel.Cor,
                Tags = viewModel.Tags.Split(';').ToList(),
                Imagem = viewModel.Imagem.Mapper(),
            };
        }

        public static CamisetaViewModel Mapper(this Camiseta model)
        {
            var tags = new StringBuilder();
            model.Tags.ForEach(x => tags.Append(x + ";"));

            return new CamisetaViewModel
            {
                Id = model.Id,
                Nome = model.Nome,
                Descricao = model.Descricao,
                Cor = model.Cor,
                Tags = tags.ToString(),
                Imagem = model.Imagem.Mapper(),
            };
        }
    }
}