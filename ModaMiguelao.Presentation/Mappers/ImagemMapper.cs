﻿using ModaMiguelao.Domain.Domain.Entities;
using ModaMiguelao.Presentation.Models;

namespace ModaMiguelao.Presentation.Mappers
{
    public static class ImagemMapper
    {
        public static Imagem Mapper(this ImagemViewModel viewModel)
        {
            return new Imagem
            {
                Id = viewModel.Id,
                Nome = viewModel.Nome,
                Url = viewModel.Url,
            };
        }

        public static ImagemViewModel Mapper(this Imagem viewModel)
        {
            return new ImagemViewModel
            {
                Id = viewModel.Id,
                Nome = viewModel.Nome,
                Url = viewModel.Url,
            };
        }
    }
}