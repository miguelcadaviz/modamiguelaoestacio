﻿using ModaMiguelao.Domain.Domain.Enums;

namespace ModaMiguelao.Presentation.Models
{
    public class CamisetaViewModel
    {
        public long Id { get; set; }
        public string Nome { get; set; }

        public string Descricao { get; set; }

        public ImagemViewModel Imagem { get; set; }

        public Cor Cor { get; set; }

        public string Tags { get; set; }
    }
}