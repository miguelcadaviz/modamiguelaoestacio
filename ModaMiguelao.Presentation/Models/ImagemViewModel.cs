﻿namespace ModaMiguelao.Presentation.Models
{
    public class ImagemViewModel
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public string Url { get; set; }
    }
}