﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ModaMiguelao.Presentation.Startup))]
namespace ModaMiguelao.Presentation
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
